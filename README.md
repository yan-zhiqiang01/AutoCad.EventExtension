# AutoCad.EventExtension

#### 介绍
一个用于在AutoCad里方便使用事件的库，由于不知道能不能搞成，所有先建个项目避免污染IFox，未来如果成了就迁移到IFox里，不成功就删除项目

目前支持的事件有：

- 系统变量修改事件
- 文档锁定事件
- 双击事件


#### 软件架构
软件架构说明


#### 安装教程

1.  fork本仓库
2.  将fork的仓库里克隆到本地
3.  添加共享项目引用

#### 使用说明

预想中的效果是
1.  在IExtensionApplication.Initialize中执行函数

```
public void Initialize()
    {
        EventFactory.UseEvent(CadEvent);
        
    }
```

2.  通过cadEvent的枚举抓取不同的事件
3.  在事件函数前添加特性即可触发

```
[SystemVariableChanged("COLORTHEME")]
    private static void Acap_SystemVariableChanged(object sender, SystemVariableChangedEventArgs e)
    {
        //COLORTHEME系统变量改变时执行

    }
```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
